const { fighter } = require('../models/fighter');

const validateExcessField = (item) => {
    let result = [];
    for (const key in item) {
        if (key === 'health' ) {
            continue;
        } 
        if (!(key in fighter) || key === 'id') {
            result.push(key);
        }        
    }
    if (result.length === 1) {
        throw Error(`Field ${result.join(', ')} is excess`)
    }
    if (result.length > 1) {
        throw Error(`Fields: ${result.join(', ')} are excess`)
    }
}

const validateMissingField = (item) => {
    let result = [];
    for (const key in fighter) {
        if (key === 'id' ) {
            continue;
        }   
        if (key === 'health' ) {
            continue;
        } 
        if (!item[key]) {
            result.push(key)
        }        
    }
    if (result.length === 1) {
        throw Error(`Field: ${result.join(', ')} is missig`)
    }
    if (result.length > 1) {
        throw Error(`Fields: ${result.join(', ')} are missig`)
    }
}

const validateOneField = (item) => {
    for (const key in fighter) {
        if (key === 'id' ) {
            continue;
        }   
        if (item[key]) {
            return;
        }        
    }
    throw Error('No update fields');
}

const validatePower = item =>  {        
    if (item.power && !(typeof item.power === 'number') || item.power <= 1 || item.power >= 100) {
        throw Error('Incorrect power');
    }
}
const validateDefense = item =>  {        
    if (item.defense && !(typeof item.defense === 'number') || item.defense  <= 1 || item.defense  >= 10) {
        throw Error('Incorrect defense');
    }
}
const validateHealth = item =>  {        
    if (item.health && !(typeof item.health === 'number') || item.health <= 80 || item.health >= 120) {
        throw Error('Incorrect health');
    }
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    
    try {
        if (!req.body) {
            throw Error('Body is undefined');
        }
        validateExcessField(req.body);
        validateMissingField(req.body);
        validatePower(req.body);
        validateDefense(req.body);
        validateHealth(req.body)
    }
    catch(err) {
        res.err = err.message;
    }
    finally {
        next();
    }        
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        if (!req.body) {
            throw Error('Body is undefined');
        }
        validateExcessField(req.body);
        validateOneField(req.body);
        validatePower(req.body);
        validateDefense(req.body);
        validateHealth(req.body)
    }
    catch(err) {
        res.err = err.message;
    }
    finally {
        next();
    }  
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;