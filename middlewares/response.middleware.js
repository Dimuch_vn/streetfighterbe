const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        let status;
        if (res.err.includes('not found')) {
            status = 404;
        } else {
            status = 400;
        }
        res.status(status).send({
            error: true,
            message: res.err
        })
    } else {
        res.json(res.data)
    }
    next();
}

exports.responseMiddleware = responseMiddleware;