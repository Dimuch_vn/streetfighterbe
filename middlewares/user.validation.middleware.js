const { user } = require('../models/user');

const validateExcessField = (item) => {
    let result = [];
    for (const key in item) {
        if (key === 'health' ) {
            continue;
        } 
        if (!(key in user) || key === 'id') {
            result.push(key)
        }        
    }
    if (result.length === 1) {
        throw Error(`Field ${result.join(', ')} is excess`)
    }
    if (result.length > 1) {
        throw Error(`Fields: ${result.join(', ')} are excess`)
    }
}


const validateMissingField = (item) => {
    let result = [];
    for (const key in user) {
        if (key === 'id' ) {
            continue;
        }   
        if (!item[key]) {
            result.push(key)
        }        
    }
    if (result.length === 1) {
        throw Error(`Field: ${result.join(', ')} is missig`)
    }
    if (result.length > 1) {
        throw Error(`Fields: ${result.join(', ')} are missig`)
    }
}

const validateOneField = (item) => {
    for (const key in user) {
        if (key === 'id' ) {
            continue;
        }   
        if (item[key]) {
            return;
        }        
    }
    throw Error('No update fields');
}

const validateEmail = item =>  {        
    const re = /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/;
    if (item.email && !re.test(String(item.email).toLowerCase())) {
        throw Error('Incorrect email');
    }
}

const validatePhoneNumber = (item) => {
    const re = /^[\+]?[(]?[380]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (item.phoneNumber && !re.test(String(item.phoneNumber).toLowerCase())) {
        throw Error('Incorrect phone number');
    }
}

const validatePassword = item =>  {        
    if (item.password && item.password.length < 3) {
        throw Error('Password must be at least 3 characters');
    }
}


const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
         if (!req.body) {
            throw Error('Body is undefined');
        }
        validateExcessField(req.body);
        validateMissingField(req.body);
        validateEmail(req.body);
        validatePhoneNumber(req.body);
        validatePassword(req.body);
    }
    catch(err) {
        res.err = err.message;
    }
    finally{        
        next();
    }  
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        if (!req.body) {
           throw Error('Body is undefined');
       }
       validateExcessField(req.body);
       validateOneField(req.body);
       validateEmail(req.body);
       validatePhoneNumber(req.body);
       validatePassword(req.body);
   }
   catch(err) {
       res.err = err.message;
   }
   finally{        
       next();
   }  
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;