const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

//create user
router.post('/', createUserValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = UserService.createUser(req.body);
        }
    }
    catch (err) {
        res.err = err.message;
    } finally {
        next();
    }

} , responseMiddleware);

//get users
router.get('/', (req, res, next) => {
    console.log('get users');
    try {
        const data = UserService.getUsers(req.body);
        res.data = data;
    }
    catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
} , responseMiddleware);

//get user
router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.getUser(req.params.id);   
    }
    catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
} , responseMiddleware);

//update user
router.put('/:id',updateUserValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = UserService.updateUser(req.params.id, req.body);
        }
    }
    catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
} , responseMiddleware);

//delete user
router.delete('/:id', (req, res, next) => {
    console.log('delete users');
    try {
        res.data = UserService.deleteUser(req.params.id);   
    }
    catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
} , responseMiddleware);

module.exports = router;