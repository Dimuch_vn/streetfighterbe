const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    createFighter(fighter) {
        if (this.search({ name: fighter.name})) {
            throw Error('User with the same name already exists');
        } else {
            if (!fighter.health) {
                fighter.health = 100;
            }
            return FighterRepository.create(fighter);
        }
    }

    getFighters() {
        return FighterRepository.getAll();
    }

    getFighter(id) {
        const fighter = this.search({ id: id});
        if (fighter) {
            return fighter;            
        } else {
            throw Error('Fighter not found');
        }
    }

    updateFighter(id, fighterData) {
        if (fighterData.name && this.search({ name: fighterData.name})) {
            throw Error('User with the same name already exists');
        }
        if (this.search({ id: id})) {
            return FighterRepository.update(id, fighterData);            
        } else {
            throw Error('Fighter not found');
        }
    }

    deleteFighter(id) {
        const fighter = this.search({ id: id});
        if (fighter) {
            return FighterRepository.delete(id);            
        } else {
            throw Error('Fighter not found');
        }
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();