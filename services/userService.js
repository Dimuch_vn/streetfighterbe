const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    createUser(user) {
        if (this.search({ email: user.email})) {
            throw Error('User with the same mail already exists');
        }
        if (this.search({ phoneNumber: user.phoneNumber})) {
            throw Error('User with the same phone number already exists');
        }
        return UserRepository.create(user);
    }

    getUsers() {
        return UserRepository.getAll();
    }

    getUser(id) {
        const user = this.search({ id: id});
        if (user) {
            return user;            
        } else {
            throw Error('User not found');
        }
    }
    
    updateUser(id, userData) {
        if (userData.email && this.search({email: userData.email})) {
            throw Error('User with the same mail already exists');
        }
        if (userData.phoneNumber && this.search({phoneNumber: userData.phoneNumber})) {
            throw Error('User with the same phone number already exists');
        }
        if (this.search({ id: id})) {
            return UserRepository.update(id, userData);            
        } else {
            throw Error('User not found');
        }
    }

    deleteUser(id) {
        const user = this.search({ id: id});
        if (user) {
            return UserRepository.delete(id);            
        } else {
            throw Error('User not found');
        }
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();